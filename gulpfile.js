const gulp = require('gulp');
const sass = require('gulp-sass');
const nano = require('gulp-cssnano');

function css() {
    return gulp.src('./resources/sass/**/*.scss', '!./resources/sass/**/_*.scss')
               .pipe(sass({includePaths: ['./resources/sass']}))
               .pipe(nano({zindex: false}))
               .pipe(gulp.dest('static/css/'));
}

function watchSass() {
    return gulp.watch('./resources/sass/**/*.scss')
               .on('change', () => gulp.parallel(css));
}

exports.css = css;
exports.watch = gulp.parallel(watchSass);
exports.default = gulp.parallel(css);
