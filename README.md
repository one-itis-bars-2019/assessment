# assessment

## Запуск проекта

0. Убедиться, что установлен Python и Node.js

1. Создать виртуальную среду

   ```bash
   python -m venv .venv
   ```
   
2. Активировать виртуальную среду и установить пакеты
   
   **Windows**
   ```cmd
   .venv\bin\Activate
   ```
   
   **macOS и Linux**
   ```bash
   source ./.venv/bin/activate
   ```
   
   ```bash
   pip install --upgrade pip
   pip install -r requirements.txt
   ```
   
   Все команды питоны (установка пакетов, запуск сервера и т. д.) следует
   запускать в виртуальной среде.
   Для выхода из виртуальной среды:
   
   ```bash
   deactivate
   ```

3. Установить зависимости npm и выполнить сборку фронтенда

   ```bash
   npm ci
   npm run build
   ```

4. Запустить проект

   ```bash
   python manage.py migrate
   python manage.py runserver
   ```

   После этого можно начинать работу.

5. Сборка фронтенда

   Если в процессе разработки меняются стили в scss, необходимо пересобрать
   фронтенд:

   ```bash
   npm run build
   ```
